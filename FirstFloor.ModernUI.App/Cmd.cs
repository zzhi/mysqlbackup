﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace FirstFloor.ModernUI.App
{
    public class Cmd
    {
        /// <summary>
        /// 执行Cmd命令
        /// </summary>
        /// <param name="workingDirectory">要启动的进程的目录</param>
        /// <param name="command">要执行的命令</param>
        public static void StartCmd(String workingDirectory, String command)
        {
            Process p = new Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.WorkingDirectory = workingDirectory;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;

            p.Start();
            p.StandardInput.WriteLine(command);
            p.StandardInput.WriteLine("exit");
            p.WaitForExit();
            p.Close();

        }

        //构建执行的命令  --verbose 不要加沉长信息,不然会很慢
        //String command = " --quick --host=localhost --default-character-set=gb2312 --lock-tables --force --port=端口号 --user=用户名 --password=密码 数据库名 -r 备份到的地址";
        /// <summary>
        /// 使用mysqldump备份程序
        /// </summary>
        /// <param name="AppPath">mysqldump.exe的目录</param>
        /// <param name="command"></param>
        public static void StartMySqldump(string AppPath, string command)
        {
            ProcessStartInfo psi = new ProcessStartInfo(AppPath + @"\mysqldump.exe");
            psi.Arguments = command;
            psi.UseShellExecute = false; // --lock-tables  --routines --force
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardError = true;
            psi.CreateNoWindow = true;
            Process pro = Process.Start(psi);
            pro.WaitForExit();
            pro.Close();
        }

        public static void StartMySql(string AppPath, string command)
        {
            ProcessStartInfo psi = new ProcessStartInfo(AppPath + @"\mysql.exe");
            psi.Arguments = command;
            psi.UseShellExecute = false; // --lock-tables  --routines --force
            psi.RedirectStandardOutput = true;
            psi.RedirectStandardInput = true;
            psi.RedirectStandardError = true;
            psi.CreateNoWindow = true;
            Process pro = Process.Start(psi);
            pro.WaitForExit();
            pro.Close();
        }


        /// <summary>
        /// 显示对话框备份mysql
        /// </summary>
        /// <param name="mysqldumpDirPath">mysqldump.exe的目录</param>
        /// <param name="cmd"></param>
        public static void BackupMysqlDialog(string mysqldumpDirPath, string cmd)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "数据备份脚本.sql|(*.sql)";
                saveFileDialog.FileName = (DateTime.Now.ToString("yyyyMMddhhmmss") + ".sql");
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    if (saveFileDialog.FileName != null)
                    {
                        string location = Application.StartupPath.ToString();
                        ProcessStartInfo psi = new ProcessStartInfo(location + @"\mysqldump.exe");
                        psi.Arguments = cmd;
                        psi.UseShellExecute = false; // --lock-tables --verbose --routines --force
                        psi.RedirectStandardOutput = true;
                        psi.RedirectStandardInput = true;
                        psi.RedirectStandardError = true;
                        psi.CreateNoWindow = true;
                        Process pro = Process.Start(psi);
                        Stream baseStream = pro.StandardOutput.BaseStream;
                        StreamReader sr = new StreamReader(baseStream, System.Text.Encoding.UTF8);
                        string strSqldump = sr.ReadToEnd();
                        FileStream fs = File.Create(saveFileDialog.FileName);
                        byte[] arr = UnicodeEncoding.UTF8.GetBytes(strSqldump);
                        fs.Write(arr, 0, arr.Length);
                        fs.Flush();
                        fs.Close();
                        pro.Close();
                        //数据库已成功备份
                        MessageBox.Show(" 数据库已成功备份 ");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" 数据库备份失败 " + ex.Message);
            }
        }

        /// <summary>
        /// 执行Cmd命令
        /// </summary>
        /// <param name="workingDirectory">要启动的进程的目录</param>
        /// <param name="command">要执行的命令</param>
        public static void StartCmd(String workingDirectory, String[] commands)
        {
            Process p = new Process();
            p.StartInfo.FileName = "cmd.exe";
            p.StartInfo.WorkingDirectory = workingDirectory;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardInput = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.CreateNoWindow = true;
            p.Start();
            foreach (string cmd in commands)
            {
                p.StandardInput.WriteLine(cmd);

            }
            p.StandardInput.WriteLine("exit");
            p.WaitForExit();
            p.Close();

        }

    }
}
