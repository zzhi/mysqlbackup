﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
namespace FirstFloor.ModernUI.App
{
    /// <summary>
    /// 
    /// </summary>
    public class DbObject
    {

        #region
        private string _dbconnectStr;

        MySqlConnection connect = new MySqlConnection();

        public DbObject()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connect"></param>
        public DbObject(string DbConnectStr)
        {
            _dbconnectStr = DbConnectStr;
            connect.ConnectionString = DbConnectStr;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SSPI"></param>
        /// <param name="Ip"></param>
        /// <param name="User"></param>
        /// <param name="Pass"></param>
        public DbObject(bool SSPI, string Ip, string User, string Pass)
        {
            connect = new MySqlConnection();
            if (SSPI)
            {
                //_dbconnectStr="Integrated Security=SSPI;Data Source="+Ip+";Initial Catalog=mysql";
                _dbconnectStr = String.Format("server={0};user id={1}; password={2}; database=mysql; pooling=false", Ip, User, Pass);
            }
            else
            {
                _dbconnectStr = String.Format("server={0};user id={1}; password={2}; database=mysql; pooling=false", Ip, User, Pass);

            }
            connect.ConnectionString = _dbconnectStr;

        }

        public string DbConnectStr
        {
            set { _dbconnectStr = value; }
            get { return _dbconnectStr; }
        }

        public string DbType
        {
            get { return "MySQL"; }
        }
        public MySqlDataReader ExecuteReader(string DbName, string strSQL)
        {
            try
            {
                OpenDB(DbName);
                MySqlCommand cmd = new MySqlCommand(strSQL, connect);
                MySqlDataReader myReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                return myReader;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// show all databases (except system databases)
        /// </summary>
        /// <returns></returns>
        public List<string> GetDBList()
        {
            List<string> dblist = new List<string>();
            string strSql = "SHOW DATABASES";
            MySqlDataReader reader = ExecuteReader("mysql", strSql);
            while (reader.Read())
            {
                dblist.Add(reader.GetString(0));
            }
            reader.Close();

            dblist.Sort(CompareStrByOrder);

            return dblist;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private int CompareStrByOrder(string x, string y)
        {
            if (x == "")
            {
                if (y == "")
                {
                    return 0;// If x is null and y is null, they're equal. 
                }
                else
                {
                    return -1;// If x is null and y is not null, y is greater. 
                }
            }
            else
            {
                if (y == "")  // ...and y is null, x is greater.
                {
                    return 1;
                }
                else
                {
                    int retval = x.CompareTo(y);
                    return retval;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DbName"></param>
        /// <returns></returns>
        private MySqlCommand OpenDB(string DbName)
        {
            try
            {
                if (connect.ConnectionString == "")
                {
                    connect.ConnectionString = _dbconnectStr;
                }
                if (connect.ConnectionString != _dbconnectStr)
                {
                    connect.Close();
                    connect.ConnectionString = _dbconnectStr;
                }
                MySqlCommand dbCommand = new MySqlCommand();
                dbCommand.Connection = connect;
                if (connect.State == System.Data.ConnectionState.Closed)
                {
                    connect.Open();
                }
                dbCommand.CommandText = "use " + DbName + "";
                dbCommand.ExecuteNonQuery();
                return dbCommand;

            }
            catch (System.Exception ex)
            {
                string str = ex.Message;
                return null;
            }

        }
        #endregion
    }
}
