﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using System.ComponentModel;
using System.Threading;


namespace FirstFloor.ModernUI.App.Pages
{
    /// <summary>
    /// Interaction logic for MysqlBackup.xaml
    /// </summary>
    public partial class MysqlBackup : UserControl
    {
        private const string characterSet = "utf8";
        private static string _databaseName = "";
        private static string _host = "localhost";
        private static string _password = "Superman";
        private static string _port = "3306";
        private static string _user = "root";
        private String appDirecroty = "";


        private string _filePath = "";
        private object _lockobject = new object();

        private string savePath = "D:\\MysqlBackup";

        private BackgroundWorker worker;//= new BackgroundWorker();
        public MysqlBackup()
        {
            InitializeComponent();


            appDirecroty = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            txthost.Text = _host;
            txtpassword.Text = _password;
            txtport.Text = _port;
            txtuser.Text = _user;

            //txtmysqlpath.Text = appDirecroty;
            txtbackupdirectory.Text = savePath;

            btnBackupstructanddate.IsEnabled = false;
            btnBackupstruck.IsEnabled = false;
            btnRestore.IsEnabled = false;

            worker = new System.ComponentModel.BackgroundWorker();
            worker.ProgressChanged += worker_ProgressChanged;
            worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            worker.DoWork += worker_DoWork;
        }

        public bool BeginProgressBar()
        {
            _host = txthost.Text.Trim();
            _password = txtpassword.Text;
            _port = txtport.Text.Trim();
            _user = txtuser.Text.Trim();

            _databaseName = cmbselecteddatabase.Text.Trim();
            if (_databaseName == "")
            {
                MessageBox.Show("请选择数据库");
                return false;
            }

            lblprompt.Visibility = Visibility.Visible;
            lblprompt.Content = "正在备份数据库，请耐心等待……";

            ProgressBar1.Visibility = Visibility.Visible;
            btnbackupdirectory.IsEnabled = false;
            btnBackupstruck.IsEnabled = false;
            btnBackupstructanddate.IsEnabled = false;

            btnTestlink.IsEnabled = false;
            btnRestore.IsEnabled = false;
            return true;

        }

        public void EndProgressBar()
        {
            lblprompt.Visibility = Visibility.Hidden;
            lblprompt.Content = "";
            this.ProgressBar1.Visibility = Visibility.Hidden;
            btnbackupdirectory.IsEnabled = true;
            btnBackupstruck.IsEnabled = true;
            btnBackupstructanddate.IsEnabled = true;
            // btnmysqlpath.IsEnabled = true;
            btnTestlink.IsEnabled = true;
            btnRestore.IsEnabled = true;
        }

        private void Backup(string path, string databaseName, bool backupdata)
        {
            try
            {

                var directory = path + "\\" + databaseName + ".sql";
                String command;
                if (backupdata)
                {
                    command = string.Format("mysqldump --quick --host={1} --default-character-set={2}  --lock-tables --routines --force --port={3} --user={4} --password={5} {6} -r \"{0}\"",
                            directory, _host, characterSet, _port, _user, _password, databaseName);
                }
                else
                {
                    command = string.Format("mysqldump --quick --host={1} --default-character-set={2} -d --lock-tables  --routines --force --port={3} --user={4} --password={5} {6} -r \"{0}\"",
                         directory, _host, characterSet, _port, _user, _password, databaseName);
                }
                Cmd.StartCmd(appDirecroty, command);

            }
            catch (Exception ex)
            {

                Metasys.Common.Logs.jobError.Info(ex.Message);
                throw new Exception(ex.Message);
            }
        }

        private void btnbackupdirectory_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new System.Windows.Forms.FolderBrowserDialog();
            var source = PresentationSource.FromVisual(this) as System.Windows.Interop.HwndSource;
            if (source != null)
            {
                System.Windows.Forms.IWin32Window win = new OldWindow(source.Handle);
                var result = dlg.ShowDialog(win);
            }
            txtbackupdirectory.Text = dlg.SelectedPath;
        }

        private void btnBackupstruck_Click(object sender, RoutedEventArgs e)
        {
            if (BeginProgressBar())
            {
                if (worker.IsBusy)
                {
                    return;
                }
                worker.RunWorkerAsync(false);

            }
        }

        private void btnBackupstructanddate_Click(object sender, RoutedEventArgs e)
        {
            if (BeginProgressBar())
            {
                if (worker.IsBusy)
                {
                    return;
                }
                worker.RunWorkerAsync(true);

            }
        }

        private void btnmysqlpath_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new System.Windows.Forms.FolderBrowserDialog();
            var source = PresentationSource.FromVisual(this) as System.Windows.Interop.HwndSource;
            if (source != null)
            {
                var win = new OldWindow(source.Handle);
                System.Windows.Forms.DialogResult result = dlg.ShowDialog(win);
            }
            //txtmysqlpath.Text = dlg.SelectedPath;
        }

        private void btnRestore_Click(object sender, RoutedEventArgs e)
        {
            var op = new Microsoft.Win32.OpenFileDialog();

            string path = @"D:\MysqlBackup";
            if (System.IO.Directory.Exists(path))
            {
                op.InitialDirectory = path;
            }
            else
            {
                op.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            op.FilterIndex = 2;
            op.RestoreDirectory = true;

            Nullable<bool> result = op.ShowDialog();
            if (result == true)
            {
                string filepath = op.FileName;
                MessageBoxResult msr = MessageBox.Show("您是否真的想覆盖以前的数据库吗？那么以前的数据库数据将丢失！！！", "警告", MessageBoxButton.OKCancel);
                if (msr == MessageBoxResult.OK)
                {

                    lblprompt.Visibility = Visibility.Visible;
                    lblprompt.Content = "正在还原数据库，请耐心等待";
                    ProgressBar1.Visibility = Visibility.Visible;
                    btnbackupdirectory.IsEnabled = false;
                    btnBackupstruck.IsEnabled = false;
                    btnBackupstructanddate.IsEnabled = false;
                    //btnmysqlpath.IsEnabled = false;
                    btnTestlink.IsEnabled = false;
                    btnRestore.IsEnabled = false;

                    worker.RunWorkerAsync(filepath);




                }
            }
        }

        private void btnTestlink_Click(object sender, RoutedEventArgs e)
        {
            _host = txthost.Text.Trim();
            _password = txtpassword.Text;
            _port = txtport.Text.Trim();
            _user = txtuser.Text.Trim();
            string constr = String.Format("server={0};uid={1}; Port={2};pwd={3}; database=mysql; pooling=false", _host, _user, _port, _password);
            try
            {
                DbObject dbhelper = new DbObject(constr);
                List<string> dblist = dbhelper.GetDBList();

                this.cmbselecteddatabase.IsEnabled = true;
                btnBackupstructanddate.IsEnabled = true;
                btnBackupstruck.IsEnabled = true;
                btnRestore.IsEnabled = true;

                this.cmbselecteddatabase.Items.Clear();

                if (dblist != null)
                {
                    if (dblist.Count > 0)
                    {
                        foreach (string dbname in dblist)
                        {
                            if (dbname != "information_schema" && dbname != "mysql" && dbname != "performance_schema" && dbname != "test")
                                this.cmbselecteddatabase.Items.Add(dbname);
                        }
                    }
                }
                MessageBox.Show("连接成功!");
                Metasys.Common.Logs.jobStatus.Info("连接成功!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CallBackUp(bool backupdata)
        {

            String fileName = (_databaseName + "-" + DateTime.Now.ToString()).Replace("-", "").Replace(":", "").Replace(" ", "").Replace("\\", "").Replace("/", "");
            string path;
            if (backupdata)
            {
                path = savePath + "\\结构和数据\\" + fileName;
            }
            else
            {
                path = savePath + "\\仅结构\\" + fileName;
            }
            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }
            _filePath = path;


            Backup(_filePath, _databaseName, backupdata);

        }

        private void Store(string filepath)
        {
            string storeName = "";
            try
            {
                int tmp = filepath.LastIndexOf('\\');
                storeName = filepath.Substring(tmp + 1, filepath.LastIndexOf('.') - filepath.LastIndexOf('\\') - 1);
                string[] cmds = new string[2];
                cmds[0] = string.Format("mysql --host={0} --default-character-set={1} --port={2} --user={3} --password={4} --execute=\"create database  if not exists {5};\"", _host, characterSet, _port, _user, _password, storeName);

                cmds[1] = string.Format("mysql --host={0} --default-character-set={1} --port={2} --user={3} --password={4} {5}<\"{6}\"", _host, characterSet, _port, _user, _password, storeName, filepath);
                Cmd.StartCmd(appDirecroty, cmds);
            }
            catch (Exception ex)
            {
                throw new Exception("还原数据库失败：" + ex.Message);
            }
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string str = e.Argument.ToString();
            if (!string.IsNullOrEmpty(str))
            {
                if (str.ToLower() == "true" || str.ToLower() == "false")
                {
                    bool b = (bool)e.Argument;

                    CallBackUp(b);
                }
                else
                {

                    Store(str);
                }
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EndProgressBar();

            MessageBox.Show("操作完成!");


        }


    }
}
